import 'package:chatbot_sdk/app/modules/main/widgets/expand/expand_controller.dart';
import 'package:get/get.dart';

import '/app/modules/main/controllers/main_controller.dart';

class MainBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<MainController>(
      () => MainController(),
      fenix: true,
    );
    Get.lazyPut<ExpandController>(
          () => ExpandController(),
      fenix: true,
    );
  }
}
