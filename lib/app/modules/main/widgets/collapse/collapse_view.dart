import 'package:flutter/material.dart';

import '../../../../core/values/app_values.dart';
import '../../../../core/widget/chatbot_icon.dart';


class Collapse extends StatelessWidget {
  Function onTap;
  Function onDragEnd;
  Function onDragUpdate;
  Collapse({Key? key, required this.onTap, required this.onDragEnd, required this.onDragUpdate}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Draggable(
      onDragEnd: (DraggableDetails detail) {
        onDragEnd(detail);
      },
      onDragUpdate: (DragUpdateDetails detail) {
        onDragUpdate(detail);
      },
      feedback:Container(
        width: AppValues.iconSizeSmall,
        height: AppValues.iconSizeSmall,
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(500)),
        child: const ChatBotIcon()
      ),
      childWhenDragging: Container(),
      child: GestureDetector(
        onTap: (){
          onTap();
        },
        child: Container(
          width: AppValues.iconSizeSmall,
          height: AppValues.iconSizeSmall,
          decoration: BoxDecoration(borderRadius: BorderRadius.circular(500)),
          child: const ChatBotIcon()
        ),
      ),
    );
  }
}
