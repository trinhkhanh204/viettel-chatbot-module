import 'dart:ffi';
import 'dart:math';

import 'package:chatbot_sdk/app/core/base/base_view.dart';
import 'package:chatbot_sdk/app/core/widget/header.dart';
import 'package:chatbot_sdk/app/modules/main/widgets/expand/select_chatbot_type_widget.dart';
import 'package:chatbot_sdk/app/modules/main/widgets/expand/user_input.dart';
import 'package:flutter/material.dart';

import '../../../../core/values/app_values.dart';
import '../../../../core/values/text_styles.dart';
import '../../../../core/widget/chatbot_icon.dart';
import 'expand_controller.dart';
import 'message_switcher/message_switcher.dart';

class Expand extends BaseView<ExpandController> {
  Expand() {
    Future.delayed(Duration(milliseconds: 300)).then((value) {
      controller.scrollToBottom();
    });
  }
  @override
  Widget body(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        const SizedBox(
          height: 100,
        ),
        GestureDetector(
          onTap: () {
            controller.onTap();
          },
          child: Container(
              margin: const EdgeInsets.only(right: AppValues.largePadding),
              width: AppValues.iconSize,
              height: AppValues.iconSize,
              decoration:
                  BoxDecoration(borderRadius: BorderRadius.circular(500)),
              child: const ChatBotIcon()),
        ),
        Expanded(
          child: Stack(children: [
            Positioned.fill(
              child: Container(
                margin: const EdgeInsets.only(top: AppValues.smallPadding),
                padding: const EdgeInsets.all(AppValues.padding),
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(AppValues.radius),
                      topRight: Radius.circular(AppValues.radius)),
                  color: Colors.white,
                ),
                child: Column(
                  children: [
                    const Header(),
                    SelectChatBotTypeWidget(
                      selectedChatBotType:
                          controller.selectedChatBotType.value,
                      onChange: (value) {
                        controller.selectedChatBotType.value = value;
                      },
                    ),
                    Expanded(child: ListView(
                      controller: controller.listViewController,
                      children: controller.messageHistory.value.map((e) => MessageSwitcherWidget(message: e)).toList(),
                    )),
                    UserInput(textEditingController: controller.textEditingController, onButtonMenuTap: () {}, onButtonMicTap: () {}, onButtonSendTap: controller.onSendMessage,)
                  ],
                ),
              ),
            ),
            Positioned(
              top: AppValues.extraSmallPadding,
              right: AppValues.padding + (AppValues.iconSize / 2),
              child: Transform.rotate(
                angle: 45 * pi / 180,
                child: Container(
                  width: 20,
                  height: 20,
                  color: Colors.white,
                ),
              ),
            )
          ]),
        )
      ],
    );
  }
}
