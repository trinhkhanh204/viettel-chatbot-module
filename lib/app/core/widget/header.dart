import 'package:flutter/material.dart';

import '../values/app_values.dart';
import '../values/text_styles.dart';
import 'chatbot_icon.dart';

class Header extends StatelessWidget {
  const Header({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(bottom: AppValues.smallPadding),
      child: const Row(
        children: [
          SizedBox(
            width: AppValues.avatarSize,
            height: AppValues.avatarSize,
            child: ChatBotIcon(),
          ),
          SizedBox(width: AppValues.smallPadding,),
          Text("Trợ lý ảo My Viettel", style: headerTextStyle,),
        ],
      ),
    );
  }
}
