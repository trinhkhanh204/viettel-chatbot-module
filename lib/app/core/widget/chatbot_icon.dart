import 'package:flutter/material.dart';

class ChatBotIcon extends StatelessWidget {
  const ChatBotIcon({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Image.asset("images/icon_chatbot.png");
  }
}
